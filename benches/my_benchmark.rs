#[macro_use]
extern crate criterion;
extern crate rayon;

use criterion::Criterion;

fn manual_threads(c: &mut Criterion) {
    c.bench_function("split 1000 / 4", |b| {
        b.iter(|| {
            let a = (0..1000).collect();
            threads_on_every_step::test_thread_pool(a, 4);
        })
    });

    c.bench_function("split 1000 / 8", |b| {
        b.iter(|| {
            let a = (0..1000).collect();
            threads_on_every_step::test_thread_pool(a, 8);
        })
    });
}

fn rayon_threads(c: &mut Criterion) {
    c.bench_function("rayon 1000", |b| {
        b.iter(|| {
            let a = (0..1000).collect();
            rayon_bench::test(a);
        })
    });
}

fn joining_children(a: &usize, b: &usize) -> usize {
    a + b
}

mod threads_on_every_step {
    use std::sync::mpsc::channel;
    use std::sync::{Arc, RwLock};
    use std::thread;

    fn coap(a: &Vec<usize>, i: u8, n: u8) -> Vec<usize> {
        let pop_len = a.len();

        let iters_at_all = pop_len * pop_len;
        let section = iters_at_all * (n as usize) / 255;

        let mut candidates = Vec::new();

        let section_start = section * (i as usize);
        let section_end = section * (i as usize + 1);

        for i in section_start..section_end {
            let (first, second) = (i / pop_len, i % pop_len);
            let child = ::joining_children(&a[first], &a[second]);

            candidates.push(child);
        }

        candidates
    }

    pub fn test_thread_pool(a: Vec<usize>, threads_count: u8) {
        let a_arc = Arc::new(RwLock::new(a));
        let (tx, rx) = channel();
        for _ in 0..10 {
            for i in 0..threads_count {
                let a = a_arc.clone();
                let tx = tx.clone();
                thread::spawn(move || {
                    tx.send(coap(&(*a.read().unwrap()), i, 255 / threads_count))
                        .unwrap();
                });
            }
            for _ in 0..threads_count {
                rx.recv().unwrap();
            }
        }
    }
}

mod rayon_bench {
    use rayon::prelude::*;

    pub fn test(a: Vec<usize>) {
        let s: Vec<_> = a
            .par_iter()
            .flat_map(|f| a.par_iter().map(move |s| ::joining_children(f, s)))
            .collect();
    }
}

criterion_group!(benches, manual_threads, rayon_threads);
criterion_main!(benches);
