use clap::ArgMatches;

/// Получение консольных аргументов
/// Обязательные аргументы:
/// 1. Путь к файлу конфигурации алгоритма (позиционный)
/// 2. Путь к файлу, в который нужно сохранить лучшую особь, полученную в результате работы алгоритма
/// 3. (опционально) Путь файла, в который писать промежуточные результаты за каждый ход цикла алгоритма
pub fn get_args_app<'a>() -> ArgMatches<'a> {
    clap_app!(myapp =>
        (long_version: "0.0.1-alpha")
        (author: "BANO.notIT <bano.notit@gmail.com>")
        (@arg input: +required "Path to the input file in JSON format")
        (@arg output: -o --output +takes_value +required "Path to the output JSON file else into STDOUT")
        (@arg stat: -s --statistics +takes_value "Path to the statistics file")
        (@arg population: -l --load +takes_value "Path to the population dump file")
    )
        .get_matches()
}
