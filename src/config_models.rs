use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use models::*;

/// Формат файла конфигурации
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    /// Список всех пар, участвующих в алгоритме
    pub pairs: Vec<Pair>,
    /// Список критериальных функций
    ///
    /// Формат записи следующий:
    /// ```json
    /// {
    ///     "<ruleName>": {
    ///         "rank": 1,
    ///         "mass": 1,
    ///         "options": ['rule_param1', 'rule_param2'],
    ///     }
    /// }
    /// ```
    pub raters: HashMap<String, Rule>,
    /// Количество дней за цикл обучения
    ///
    /// В школах цикл составляет неделю из 5-6 рабочих дней, в других заведениях 2 недели: ~12 дней
    pub days: usize,
    /// Количество аудиторий
    /// Это поле заполняется автоматически функицей конфигуратором, на основе списка аудиторий
    pub rooms: usize,

    /// Максимальное количество занятий в день
    ///
    /// В школе обычно 7-8 уроков, но алгоритм требует места "для манёвров",
    ///  поэтому оптимальным для школы будет значение 10
    #[serde(rename = "lessonsPerDay")]
    pub lessons_per_day: usize,

    /// Продвинутые настройки
    #[serde(default)]
    pub advanced: Advanced,
}

/// Учебная "пара"
/// Пара является единицей, отвечающей за соответствие учебного процесса и плана
#[derive(Debug, Serialize, Deserialize)]
pub struct Pair {
    /// Количество уроков за цикл
    pub count: usize,
    /// Преподаватель, ведущий урок
    pub teacher: TeacherId,
    /// Группа
    pub group: GroupId,
}

/// Критериальная запись
/// Хранит настройки к выбранной критериальной функции.
#[derive(Debug, Serialize, Deserialize)]
pub struct Rule {
    /// Коэффициент "массы" влияния результата функции на оценку особи.
    /// `result = mass * fn(...)`
    /// Всегда стоит ставить в 1. Если будет равен 0, то функция не будет участвовать в оценке,
    /// но будет вычисляться при оценке, поэтому лучше просто удалить запись полностью.
    ///
    /// Отрицательные значения стоит выставлять, только если нужно "инвертировать" поведение функции.
    #[serde(default = "get_one")]
    pub mass: u32,

    /// Ранг функции
    /// Может быть неограниченное количество рангов критериев. Чем меньше ранг -
    /// тем более важным он считается, но минимальным является нулевой (0) ранг.
    ///
    /// По умолчанию ставится нулевой - самый высший ранг.
    ///
    /// При выборе рангов лучше всего использовать 3 ранговую схему:
    /// - 0 - для функций, которые обязательны к выполнению в любом случае, например деление групп на страты и привязка к аудиториям. Лучше всего ставить функции, которые считают количество плохих свойств, вроде дырок в расписании.
    /// - 1 - для функций, выполнение которых было бы очень желательно для всех. Например длинна учебного дня или количество последовательных уроков.
    /// - 2 - для функций, отвечающих за "хотелки", например количество "методических дней" у преподавателей или количество их переходов между аудиториями
    #[serde(default = "get_zero")]
    pub rank: usize,

    /// Опции функции
    /// Опции, передаваемые функции, чтобы настроить определённое поведение. Каждая функция, принимающая какие-либо опции имеет документацию по их формату
    pub options: Option<Vec<String>>,
}

impl Config {
    /// Получение всех возможных RoomKey
    /// Может пригодиться при смешивании особей
    pub fn gen_room_keys(&self) -> Vec<RoomKey> {
        let mut result = Vec::with_capacity(self.days * self.lessons_per_day * self.rooms);

        for day in 0..self.days {
            for lesson in 0..self.lessons_per_day {
                for room in 0..self.rooms {
                    result.push(RoomKey(day, lesson, room))
                }
            }
        }

        result
    }

    /// Получение всех пар с учётом их количества
    /// Если в секции `pairs` была пара (1, 1, 15), то этот метод выдаст 15 пар (1, 1)
    ///
    /// Используется для генерации случайной особи
    pub fn get_pairs(&self) -> Vec<LessonPair> {
        self.pairs
            .iter()
            .map(|a| {
                let mut clones = Vec::with_capacity(a.count);
                for _ in 0..a.count {
                    clones.push(LessonPair(a.teacher, a.group))
                }
                clones
            })
            .flatten()
            .collect()
    }

    /// Получить количество рангов для функции оценки
    ///
    /// Возвращает номер максимального ранга для составления конечной функции оценки.
    pub fn get_ranks_count(&self) -> usize {
        use itertools::max;
        max(self.raters.iter().map(|(_, rule)| rule.rank)).unwrap() + 1
    }
}

/// Продвинутые настройки алгоритма генерации
///
/// ВНИМАНИЕ! Все настройки и имена в конфигурационном файле должны быть в вербллюжъейНотации!
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Advanced {
    /// Объём популяции. По умолчанию 100
    #[serde(default = "get_default_population_size")]
    pub base_population_size: usize,
    /// Количество мутаций для ребёнка с худшими родителям. По умолчанию 15
    #[serde(default = "get_default_mutations_rate")]
    pub mutations_per_child: u8,

    /// Процент сходимости, при котором симуляция прекращается. По умолчанию 90
    #[serde(default = "get_default_convergence")]
    pub breaking_convergence: usize,

    /// Частота дампа всей популяции. Измеряется в циклах. Если `0`, то дамп не производится.
    /// По умолчанию 100.
    #[serde(default = "get_default_dump_freq")]
    pub dump_freq: u64,

    /// Файл, в который сохраняется дамп популяции. По умолчанию population-dump.json.
    #[serde(default = "get_default_dump_file")]
    pub dump_file: String,
}

impl Default for Advanced {
    fn default() -> Self {
        Self {
            base_population_size: get_default_population_size(),
            mutations_per_child: get_default_mutations_rate(),
            breaking_convergence: get_default_convergence(),
            dump_freq: get_default_dump_freq(),
            dump_file: get_default_dump_file(),
        }
    }
}

fn get_default_population_size() -> usize {
    100
}

fn get_default_dump_freq() -> u64 {
    100
}

fn get_default_dump_file() -> String {
    "population-dump.json".to_owned()
}

fn get_default_convergence() -> usize {
    90
}

fn get_default_mutations_rate() -> u8 {
    15
}

fn get_zero() -> usize {
    0
}

fn get_one() -> u32 {
    1
}
