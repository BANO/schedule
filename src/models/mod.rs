/// Список примитивов, используемых во всей программе
mod prims;
/// Класс особи
mod schedule;
/// Интерфейсы, необходимые для взаимодействия с особью и примитивами
mod traits;

#[cfg(test)]
mod test;

pub use self::prims::*;
pub use self::schedule::Schedule;
pub use self::traits::Coition;
pub use self::traits::Diff;
pub use self::traits::Setter;
