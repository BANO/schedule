use self::super::traits::Diff;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

/// Идентификатор учителя
pub type TeacherId = usize;
/// Идентификатор группы
pub type GroupId = usize;
/// Идентификатор аудитории
pub type ClassroomId = usize;
/// В кабинете может происходить только один урок, соответственно его и храним
pub type Lessons = HashMap<ClassroomId, LessonPair>;
/// Один день в сетке расписания состоит из нескольких уроков
pub type Day = Vec<Lessons>;

/// Одно назначение группы и преподавателя
/// Оно является ячейкой в таблице расписания
#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug, Serialize, Deserialize, Ord, PartialOrd)]
pub struct LessonPair(pub TeacherId, pub GroupId);

/// Место одного урока, включает в себя номер дня, номер урока и id аудитории
/// Используется для обращения к определённым урокам
#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug, Serialize, Deserialize, Ord, PartialOrd)]
pub struct RoomKey(pub usize, pub usize, pub ClassroomId);

impl Diff for Day {
    type Item = LessonPair;
    type Key = (usize, ClassroomId);

    fn diff(&self, day_b: &Day) -> (Vec<(usize, ClassroomId)>, Vec<LessonPair>) {
        let mut classrooms = vec![];
        let mut lessons = vec![];

        for lesson_id in 0..std::cmp::max(self.len(), day_b.len()) {
            let a: Option<&Lessons> = self.get(lesson_id);
            let b: Option<&Lessons> = day_b.get(lesson_id);

            // Узнаем, если есть они равны и всегда сохраняем правый урок.

            match (a, b) {
                (None, None) => (), // Вот такого просто быть не может, но на всякий случай

                (None, Some(b)) => {
                    for &room_id in b.keys() {
                        classrooms.push((lesson_id, room_id))
                    }
                }

                (Some(a), None) => {
                    for (&room_id, &lesson) in a {
                        classrooms.push((lesson_id, room_id));
                        lessons.push(lesson);
                    }
                }

                (Some(a), Some(b)) => {
                    let (rooms, mut lessons_differed) = a.diff(&b);

                    for &room_id in &rooms {
                        classrooms.push((lesson_id, room_id));
                    }

                    lessons.append(&mut lessons_differed);
                }
            };
        }

        (classrooms, lessons)
    }
}

impl Diff for Lessons {
    type Item = LessonPair;
    type Key = usize;

    fn diff(&self, lesson_b: &Lessons) -> (Vec<usize>, Vec<LessonPair>) {
        let mut classrooms = vec![];
        let mut lessons = vec![];

        let merged_keys = self.keys().merge(lesson_b.keys());
        let mut keys = HashSet::new();

        for key in merged_keys {
            keys.insert(key);
        }

        for key in keys {
            let a = self.get(key);
            let b = lesson_b.get(key);

            // Узнаем, если есть они равны и всегда сохраняем правый урок.

            match (a, b) {
                (None, None) => (), // Вот такого просто быть не может, но на всякий случай

                (None, Some(_)) => classrooms.push(*key),
                (Some(a), None) => {
                    classrooms.push(*key);
                    lessons.push(a.clone())
                }
                (Some(a), Some(b)) => {
                    if a != b {
                        classrooms.push(*key);
                        lessons.push(a.clone())
                    }
                }
            };
        }

        (classrooms, lessons)
    }
}
