use self::super::*;
use serde_json;

#[test]
fn test_parsing_from_json() {
    let schedule: Schedule =
        serde_json::from_str(r#"{"days": [ [ { "1": [1, 2] }, { "2": [1, 3], "3": [2, 1] } ] ]}"#)
            .unwrap();

    assert_eq!(schedule.get_item(RoomKey(0, 0, 1)), Some(&LessonPair(1, 2)));
}

#[test]
fn test_getting_common() {
    let schedule_json = vec![
        r#"{"days": [ [ { "1": [1, 2]              }, { "2": [1, 3], "3": [2, 1] } ] ]}"#,
        r#"{"days": [ [ { "1": [1, 2], "2": [2, 3] }, { "2": [1, 3]              } ] ]}"#,
        r#"{"days": [ [ { "1": [1, 2]              }, { "2": [1, 3]              } ] ]}"#,
    ];

    let s: Vec<Schedule> = schedule_json
        .into_iter()
        .map(|a| serde_json::from_str::<Schedule>(a).unwrap())
        .collect();

    println!("a {:?}", s[0].days);

    let (common_schedule, _diff) = s[0].get_common_diff(&s[1]);
    assert_eq!(common_schedule, s[2])
}

#[test]
fn getting_pairs() {
    let schedule: Schedule =
        serde_json::from_str(r#"{"days": [ [ { "1": [1, 2] }, { "2": [1, 3], "3": [2, 1] } ] ]}"#)
            .unwrap();

    let mut pairs = schedule.get_pairs();

    pairs.sort();

    assert_eq!(
        pairs,
        vec![
            (RoomKey(0, 0, 1), LessonPair(1, 2)),
            (RoomKey(0, 1, 2), LessonPair(1, 3)),
            (RoomKey(0, 1, 3), LessonPair(2, 1)),
        ]
    )
}

mod check_inserting {
    use super::{LessonPair, RoomKey};

    fn get_schedule() -> super::Schedule {
        serde_json::from_str(r#"{"days": [ [ { "1": [1, 2] }, { "2": [1, 3], "3": [2, 1] } ] ]}"#)
            .unwrap()
    }

    #[test]
    fn outbouds_day() {
        let schedule = get_schedule();
        let item = LessonPair(0, 0);

        assert_eq!(schedule.can_insert(item, RoomKey(1, 0, 1)), false);
        assert_eq!(schedule.can_insert(item, RoomKey(0, 0, 2)), true);
    }

    #[test]
    fn same_room() {
        let schedule = get_schedule();
        let item = LessonPair(0, 0);

        assert_eq!(schedule.can_insert(item, RoomKey(0, 0, 1)), false);
        assert_eq!(schedule.can_insert(item, RoomKey(0, 0, 2)), true);
    }

    #[test]
    fn locked_teacher() {
        let schedule = get_schedule();
        let key = RoomKey(0, 0, 2);

        assert_eq!(schedule.can_insert(LessonPair(1, 0), key), false);
        assert_eq!(schedule.can_insert(LessonPair(2, 0), key), true);
    }

    #[test]
    fn locked_group() {
        let schedule = get_schedule();
        let key = RoomKey(0, 0, 2);

        assert_eq!(schedule.can_insert(LessonPair(0, 2), key), false);
        assert_eq!(schedule.can_insert(LessonPair(0, 1), key), true);
    }
}

mod inserting {
    use super::{traits::Setter, LessonPair, RoomKey};

    fn get_schedule() -> super::Schedule {
        serde_json::from_str(
            r#"{"days": [ [ { "1": [1, 2] }, { "2": [1, 3], "3": [2, 1] } ], [] ]}"#,
        )
        .unwrap()
    }

    #[test]
    fn fails_on_day_outbound() {
        let mut schedule = get_schedule();
        let item = LessonPair(0, 0);

        assert_eq!(schedule.insert_item(RoomKey(2, 0, 1), item), Err(()));
        assert_eq!(schedule.insert_item(RoomKey(0, 0, 0), item), Ok(()));
    }

    #[test]
    fn adds_lessons_to_day() {
        let mut schedule = get_schedule();
        let item = LessonPair(0, 0);

        assert_eq!(schedule.insert_item(RoomKey(0, 100, 1), item), Ok(()));
        assert_eq!(schedule.insert_item(RoomKey(0, 1000, 0), item), Ok(()));
    }

    #[test]
    fn fails_if_no_room_vocation() {
        let mut schedule = get_schedule();
        let item = LessonPair(0, 0);

        assert_eq!(schedule.insert_item(RoomKey(0, 0, 1), item), Err(()));
        assert_eq!(schedule.insert_item(RoomKey(0, 0, 0), item), Ok(()));
    }
}
