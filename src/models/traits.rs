/// Интерфейс для особи и примитивов, реализующий возможность "вставки" значений по определённому индексу
pub trait Setter {
    type Item;
    type Key;

    /// Метод вставки значения по определённому индексу
    fn insert_item(&mut self, key: Self::Key, item: Self::Item) -> Result<(), ()>;
}

/// Интерфейс для особи и примитивов, реализующий нахождение разности
pub trait Diff {
    type Item;
    type Key;
    /// Метод, получающий на вход другой объект или примитив и возвращающий ключи, которые различаются в двух объектах, и значения, хранящиеся по этим ключам
    fn diff(&self, other: &Self) -> (Vec<Self::Key>, Vec<Self::Item>);
}

/// Интерфейс слияния двух особей или примитивов
pub trait Coition: Clone + Diff {
    /// Мутод удаления значений по их ключам
    fn remove_keys(&mut self, diff: &Vec<Self::Key>) -> Vec<Option<Self::Item>>;

    /// Получение новой особи, сохраняющей общие свойства и список различающихся свойств
    fn get_common_diff(&self, other: &Self) -> (Self, Vec<Self::Item>) {
        let mut child = self.clone();

        let diff = self.diff(other);

        child.remove_keys(&diff.0);

        (child, diff.1)
    }
}
