use models::Schedule;
use std::cmp::Ordering;
use std::fmt;

/// В популяции хранится значение оценивающией функции для особи и ссылка на особь в куче
#[derive(Debug)]
pub struct PopulationItem(pub Vec<f32>, pub Box<Schedule>);

impl Ord for PopulationItem {
    fn cmp(&self, other: &Self) -> Ordering {
        for (&a, &b) in self.0.iter().zip(other.0.iter()) {
            if a > b {
                return Ordering::Greater;
            } else if a < b {
                return Ordering::Less;
            }
        }

        Ordering::Equal
    }
}

impl Eq for PopulationItem {}

impl PartialOrd for PopulationItem {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for PopulationItem {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl fmt::Display for PopulationItem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ratings_as_str: Vec<String> = self.0.iter().map(|&a| (a * 100.0).to_string()).collect();

        write!(f, "[{}]", ratings_as_str.join(", "))
    }
}

/// Популяция
/// В популяции все особи хранятся в обратном порядке сортировки,
/// чтобы при вставке новой особи приходилось меньше двигать объекты в памяти
pub type Population = Vec<PopulationItem>;

/// Функция вставки новой особи в популяцию
pub fn insert_and_shift(population: &mut Population, item: PopulationItem) {
    let orig_len = population.len();

    population.push(item);

    // Соблюлдаем обратную сортироку обязательно
    population.sort_by(|a, b| a.cmp(&b).reverse());

    // Удаляем особь, которая оказалась в самом низу цепочки
    population.truncate(orig_len);
}
