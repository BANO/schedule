use std::collections::BTreeMap;

use crate::models::{GroupId, LessonPair, RoomKey, Schedule};

use self::super::traits::RatingFn;
use config_models::Config;
use ratings::utils::get_group_lessons_count;

/// # Уменьшение количества окон у учеников (`lessonsSeq`)
/// Считает количество идущих подряд уроков. Должно уменьшать количество окон у учеников.
pub struct LessonsSeq(pub f32);

impl RatingFn for LessonsSeq {
    fn new(conf: &Config, _: &Vec<String>) -> Box<Self> {
        let best = get_group_lessons_count(conf)
            .iter()
            .fold(0, |sum, (_, &count)| sum + count - 1);

        Box::new(Self(best as f32))
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut result = 0.0;
        let mut map: BTreeMap<GroupId, usize> = BTreeMap::new();

        for (RoomKey(_, lesson, _), LessonPair(_, gid)) in &schd.get_pairs() {
            if map.contains_key(gid) {
                let lesson = *lesson;
                let last_lesson = map.get_mut(gid).unwrap();

                if lesson > 0 && *last_lesson == lesson - 1 {
                    result += 1.0;
                }

                *last_lesson = lesson;
            } else {
                map.insert(*gid, *lesson);
            }
        }

        result / self.0
    }
}

#[test]
fn rater() {
    use serde_json;

    let a: Schedule = serde_json::from_str(
        r#"{"days": [ [ { "1": [1, 1] }, { "2": [2, 1], "3": [1, 3] },{},{"3": [1, 2] } ] ]}"#,
    )
    .unwrap();

    let rater = LessonsSeq(4.0);

    assert_eq!(rater.rate(&a), 0.25);
}

#[test]
fn initialization() {
    use serde_json;

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":0,"group":0,"count":1},{"teacher":0,"group":1,"count":5}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = LessonsSeq::new(&conf, &vec![]);

    assert_eq!(rater.0, 4.0);
}
