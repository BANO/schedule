use std::collections::HashMap;

use models::{LessonPair, RoomKey, Schedule, TeacherId};

use self::super::traits::RatingFn;
use config_models::{Config, Pair};

/// # Методические дни (`teacherFreeDay`)
/// Считает количество методических дней для учителей
pub struct TeacherFreeDay(pub f32);

impl RatingFn for TeacherFreeDay {
    fn new(conf: &Config, _: &Vec<String>) -> Box<Self> {
        let mut lessons_for_teacher: HashMap<u32, u32> = HashMap::new();
        for &Pair { count, teacher, .. } in &conf.pairs {
            let count = count as u32;
            let teacher = teacher as u32;
            if lessons_for_teacher.contains_key(&teacher) {
                let already = lessons_for_teacher[&teacher];
                lessons_for_teacher.insert(teacher, count + already)
            } else {
                lessons_for_teacher.insert(teacher, count)
            };
        }

        let lessons_per_day = conf.lessons_per_day as u32;
        let max_days = conf.days as u32;

        let ceil = |a, b| 1 + ((a - 1) / b);

        let best = lessons_for_teacher.iter().fold(0, |sum, (_, &count)| {
            let teacher_used_days = ceil(count, lessons_per_day);

            sum + (max_days - teacher_used_days)
        });

        Box::new(Self(best as f32))
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut map: HashMap<TeacherId, Vec<usize>> = HashMap::new();

        for (RoomKey(day, _, _), LessonPair(tid, _)) in &schd.get_pairs() {
            if !map.contains_key(tid) {
                map.insert(*tid, vec![*day]);
            } else {
                let days = map.get_mut(tid).unwrap();
                if !days.contains(day) {
                    days.push(*day);
                }
            }
        }
        let mut result: u32 = 0;

        let max_days = schd.days.len() as u32;

        for (_, days) in map.iter() {
            result += max_days - days.len() as u32;
        }

        result as f32 / self.0
    }
}

#[test]
fn rater() {
    use serde_json;

    // учитель 1 работает 2 дня: первый и второй, учитель 2 работает только в первый, второй у него свободный
    let a: Schedule = serde_json::from_str(
        r#"{ "days": [
                [ { "1": [1, 1] }, { "2": [2, 1], "3": [1, 3] }],
                [ { "3": [1, 2] } ]
            ] }"#,
    )
    .unwrap();

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":0,"group":0,"count":1},{"teacher":0,"group":1,"count":5}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = TeacherFreeDay::new(&conf, &vec![]);

    assert_eq!(rater.rate(&a), 2.0 / 2.0);
}

#[test]
fn initialization() {
    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":0,"group":0,"count":1},{"teacher":0,"group":1,"count":5}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = TeacherFreeDay::new(&conf, &vec![]);

    assert_eq!(rater.0, 1.0);
}
