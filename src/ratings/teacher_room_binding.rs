use std::collections::BTreeMap;

use models::{ClassroomId, LessonPair, RoomKey, Schedule, TeacherId};

use self::super::traits::RatingFn;
use config_models::{Config, Pair};

/// # Привязка кабинетов к учителям (`teacherRoomBinding`)
/// Считает количества уроков, которые учитель с кабинетом провёл вне своего кабинета.
/// Формат опций: "<id учителя> -> <id кабинета>", пробелы рядом вокруг стрелочки обязательны.
pub struct TeacherRoomBinding {
    teacher_room: BTreeMap<TeacherId, ClassroomId>,
    pub best: f32,
}

impl RatingFn for TeacherRoomBinding {
    fn new(conf: &Config, options: &Vec<String>) -> Box<Self> {
        let mut map = BTreeMap::new();

        for (i, option) in options.iter().enumerate() {
            let nums: Vec<&str> = option.split(" -> ").collect();

            if nums.len() != 2 {
                eprintln!(
                    "Skipping {}-th option of \"teacher-room-binding\" 'case of wrong format",
                    i
                );
                continue;
            }

            let tid: TeacherId = match nums[0].parse::<usize>() {
                Ok(tid) => tid,
                Err(e) => {
                    eprintln!(
                        "Error while parsing TID option {} of \"teacher-room-binding\" {:}",
                        i, e
                    );
                    eprintln!("Skipping option {}", i);
                    continue;
                }
            };
            let rid: ClassroomId = match nums[1].parse::<usize>() {
                Ok(tid) => tid,
                Err(e) => {
                    eprintln!(
                        "Error while parsing RID option {} of \"teacher-room-binding\" {:}",
                        i, e
                    );
                    eprintln!("Skipping option {}", i);
                    continue;
                }
            };

            map.insert(tid, rid);
        }

        let best = conf
            .pairs
            .iter()
            .fold(0, |sum, &Pair { teacher, count, .. }| {
                if map.contains_key(&teacher) {
                    sum + count as u32
                } else {
                    sum
                }
            });

        Box::new(Self {
            teacher_room: map,
            best: best as f32,
        })
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut result = 0.0;

        for (RoomKey(_, _, rid), LessonPair(tid, _)) in &schd.get_pairs() {
            if self.teacher_room.get(tid) == Some(rid) {
                result += 1.0;
            }
        }

        result / self.best
    }
}

#[test]
fn rater() {
    use serde_json;

    // учитель 1 и 2 проводят уроки в своих кабинетах
    let a: Schedule = serde_json::from_str(
        r#"{"days": [ [ { "1": [1, 1] }, { "2": [2, 1], "3": [1, 3] },{"2":[1,1]},{"3": [1, 2] } ] ]}"#,
    )
        .unwrap();
    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":1,"group":0,"count":10},{"teacher":2,"group":1,"count":10}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = TeacherRoomBinding::new(&conf, &vec!["1 -> 1".to_string(), "2 -> 2".to_string()]);

    assert_eq!(rater.rate(&a), 2.0 / 20.0);
}

#[test]
fn initialization() {
    use serde_json;

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":1,"group":0,"count":10},{"teacher":2,"group":1,"count":10}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = TeacherRoomBinding::new(&conf, &vec!["1 -> 1".to_owned(), "2 -> 2".to_owned()]);

    assert_eq!(rater.best, 20.0);
}
