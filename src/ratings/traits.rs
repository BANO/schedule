use crate::config_models::Config;
use crate::models::Schedule;

pub trait RatingFn: Sync + Send {
    fn new(conf: &Config, options: &Vec<String>) -> Box<Self>
    where
        Self: Sized;

    fn rate(&self, schd: &Schedule) -> f32;
}
