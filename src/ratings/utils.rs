use config_models::{Config, Pair};
use models::GroupId;
use std::collections::HashMap;

pub fn get_group_lessons_count(conf: &Config) -> HashMap<GroupId, u32> {
    let mut result = HashMap::new();

    for &Pair { group, count, .. } in &conf.pairs {
        if result.contains_key(&group) {
            let already = result[&group];
            result.insert(group, already + count as u32);
        } else {
            result.insert(group, count as u32);
        }
    }

    result
}
