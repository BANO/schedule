use itertools::Itertools;
use serde_json;

use config_models::Config;
use models::{LessonPair, RoomKey, Schedule};

#[test]
fn same_pairs_in_result_after_coition() {
    let a =
        serde_json::from_str(r#"{"days": [ [ { "1": [1, 2] }, { "2": [1, 3], "3": [2, 1] } ] ]}"#)
            .unwrap();
    let b =
        serde_json::from_str(r#"{"days": [ [ { "1": [1, 2] }, { "2": [2, 1], "3": [1, 3] } ] ]}"#)
            .unwrap();

    let config: Config = serde_json::from_str(
        r#"
            {
                "pairs": [],
                "raters": { },
                "days": 5,
                "rooms": 10,
                "lessonsPerDay": 10
            }"#,
    )
    .unwrap();

    Schedule::set_available_room_keys(config.gen_room_keys());

    let c = Schedule::from_parents(&a, &b);

    //    Schedule::set_available_room_keys(vec![]);

    let first_pairs = a.get_pairs().iter().map(|a| a.1).sorted();
    let last_pairs = c.get_pairs().iter().map(|a| a.1).sorted();

    assert_eq!(first_pairs, last_pairs);

    // Check common part
    assert_eq!(c.get_item(RoomKey(0, 0, 1)).unwrap(), &LessonPair(1, 2))
}
