import argparse
import sys
from json import loads

import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('input', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
parser.add_argument('output', nargs='?', default='ratings-plot.png')

args = parser.parse_args()

maxes, mids, mins = [], [], []
for line in args.input:
    line = line.strip()

    high, mid, low = line.split('/')
    maxes.append(loads(high))
    mids.append(loads(mid))
    mins.append(loads(low))

rates_count = len(maxes[0])

print('Plotting {} rates in {} cycles'.format(rates_count, len(maxes)))

maxes = np.transpose(maxes)
mids = np.transpose(mids)
mins = np.transpose(mins)

fig = plt.figure()
gs = fig.add_gridspec(rates_count, 1)

for rate_n in range(rates_count):
    ax = fig.add_subplot(gs[rate_n, 0])

    ax.plot(maxes[rate_n], label='max')
    ax.plot(mids[rate_n], label='med')
    ax.plot(mins[rate_n], label='min')

    ax.grid(True)

    ax.legend(framealpha=1, fontsize='xx-small')

    ax.spines['top'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(top=False, right=False)

    ax.set_title('Rang #' + str(rate_n + 1))
    ax.set_xlabel('cycles')
    ax.set_ylabel('ideality, %')

fig.tight_layout()

print('Saving to {0}'.format(args.output))

fig.savefig(args.output)
# plt.show()
